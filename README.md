# GitOut

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/GitOut.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/GitOut.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/GitOut.jl/-/pipelines)

A simple API built around `git` CLI, built around the principle of *respecting your commandline
globals* (e.g. `git` and `ssh` configs).  Can use either system git or `Git_jll`.

## Example
```julia
using GitOut

r = clone("https://gitlab.com/ExpandingMan/GitOut.jl.git")  # returns Repo object

printstatus(r)  # prints git status output

open(joinpath(pathof(r), "newfile"), write=true) do io
    write(io, "what up!")
end

add(r)

commit(r, message="created a useless file")

push(r)  # hopefully this will fail in this example!
```
