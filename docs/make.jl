using GitOut
using Documenter

DocMeta.setdocmeta!(GitOut, :DocTestSetup, :(using GitOut); recursive=true)

makedocs(;
    modules=[GitOut],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/GitOut.jl/blob/{commit}{path}#{line}",
    sitename="GitOut.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/GitOut.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "API" => "api.md",
    ],
)
