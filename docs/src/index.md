```@meta
CurrentModule = GitOut
```

# [GitOut](https://gitlab.com/ExpandingMan/GitOut.jl)

A simple Julia wrapper of command-line `git`.  The reason to use `GitOut` rather than a true library
such as `LibGit2` is that it respects global configuration such as that set in `~/.gitconfig` or
`~/.ssh/config`.

## Example
```julia
using GitOut

r = clone("https://gitlab.com/ExpandingMan/GitOut.jl.git")  # returns Repo object

printstatus(r)  # prints git status output

open(joinpath(pathof(r), "newfile"), write=true) do io
    write(io, "what up!")
end

add(r)

commit(r, message="created a useless file")

push(r)  # hopefully this will fail in this example!
```


## TODO

- [ ] Git configuration API.
- [ ] More complete library functions.
- [ ] More testing.
