```@meta
CurrentModule = GitOut
```

# API

```@index
```

```@docs
GitOut
```


## Command Interface
```@docs
use_system_git!
git
rungit
```


## Repository
```@docs
Repo
clone
remotenames
getremoteurl
remotes
addremote
rawstatus
status
printstatus
revlist
listcommits
pull
add
remove
commit
push
init
pathof(::Repo)
```


## Internals
```@docs
appendargs!
```
