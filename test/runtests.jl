using GitOut
using Test

const git_config_file = joinpath(@__DIR__,"gitconfig")
ENV["GIT_CONFIG_GLOBAL"] = git_config_file
ENV["GIT_CONFIG_SYSTEM"] = git_config_file

@testset "GitOut.jl" begin

@testset "new_repo" begin
    dir = mktempdir()

    r = init(dir)
    @test isdir(joinpath(dir, ".git"))
    @test abspath(dir) == abspath(pathof(r))

    open(io -> write(io, "what up"), joinpath(pathof(r),"newfile"), write=true)
    @test collect(status(r)) == [["??", "newfile"]]

    add(r)
    @test collect(status(r)) == [["A", "newfile"]]

    commit(r, message="test commit")
    @test length(listcommits(r)) == 1

    rm(dir, recursive=true, force=true)
end

@testset "clone" begin
    dir = mktempdir()

    cd(dir) do
        r = clone("https://gitlab.com/ExpandingMan/GitOut.jl.git", "GitOut")
        @test isdir("GitOut")
    
        cd("GitOut") do
            @test isfile("Project.toml")
            @test isdir("src")
        end
    
        @test GitOut.currentbranch(r) == "main"
    
        # this is just to test that it doesn't error
        @test pull(r) isa Repo
    end

    rm(dir,  recursive=true, force=true)
end

end
