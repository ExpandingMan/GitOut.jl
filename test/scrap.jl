import Pkg; Pkg.activate(joinpath(@__DIR__,".."))
using GitOut
using URIs
using Test

using GitOut; const GO = GitOut

ENV["JULIA_DEBUG"] = "GitOut"

scrap() = quote
    dir = mktempdir()
    cd(dir)

    r = clone("https://gitlab.com/ExpandingMan/GitOut.jl.git", "GitOut")
end

