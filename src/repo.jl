

"""
    $TYPEDEF

Data structure representing a git repository.  This is associated with a particular path on the local file system,
so it becomes invalid if the path is deleted.

## Examples
```julia
r = Repo("/path/to/repo")  # an existing repo
r = clone("https://path/to/repo.git", "newrepo")  # clone from remote and return repo

r = Repo("/phat/to/NOT/repo")  # will error on non-repo unless validate=false

mkdir("newnewrepo")
r = init("newnewrepo")  # create a new git repo
```
"""
struct Repo
    localpath::String  # this will always be stored as abspath

    function Repo(path::AbstractString; validate::Bool=true) 
        path = abspath(path)
        if validate && !isgitrepo(path)
            throw(ArgumentError("$path is not a git repo. Pass `validate=false` to create Repo object anyway, "*
                                "or use `init` or `clone` instead."))
        end
        new(abspath(path))
    end
end

"""
    $TYPEDSIGNATURES

Determine if directory `dir` can be a git repo by checking for a `.git` subdirectory.
"""
isgitrepo(dir::AbstractString) = isdir(joinpath(dir,".git"))

"""
    $TYPEDSIGNATURES

Returns the path to the git repo `r` on the local filesystem.
"""
Base.pathof(r::Repo) = r.localpath

"""
    $TYPEDSIGNATURES

Create a `Cmd` object for running a git command for the repo `r`.  $(_docstring_kwarg_notice("git")) 
"""
git(r::Repo, args=(;); kw...) = git(args; dir=pathof(r), kw...)

"""
    $TYPEDSIGNATURES

Run a git command for the repo `r`.  $(_docstring_kwarg_notice())
"""
rungit(r::Repo, args=(;); kw...) = rungit(args; dir=pathof(r), kw...)

"""
    $TYPEDSIGNATURES

Clone the git repo from remote at `url` to path `path`.  $(_docstring_kwarg_notice())
"""
function clone(url::URI, path::AbstractString; kw...)
    cmd = ["clone", string(url)]
    isnothing(path) || push!(cmd, path)
    rungit(cmd; kw...)
    Repo(path, validate=false)
end
clone(url::AbstractString, path::AbstractString; kw...) = clone(URI(url), path; kw...)

"""
    $TYPEDSIGNATURES

Retrieve a list of remotes by name as an `AbstractVector` of `AbstractString`.  $(_docstring_kwarg_notice())
"""
function remotenames(r::Repo; kw...)
    o = rungit(r, ["remote"]; kw...)
    split(o, "\n", keepempty=false)
end

"""
    $TYPEDSIGNATURES

Get the URL for the remote of repo `r` named `name`.  $(_docstring_kwarg_notice())
"""
getremoteurl(r::Repo, name::AbstractString; kw...) = rungit(r, ["remote", "get-url", n]; kw...)

defaultremote(r::Repo; kw...)  = remotenames(r; kw...) |> first

"""
    $TYPEDSIGNATURES

Get the remotes of repo `r` as an `OrderedDict` the keys of which are the remote names and the values of which
are the remote URL's.  $(_docstring_kwarg_notice())
"""
function remotes(r::Repo; kw...)
    ns = remotenames(r; kw...)
    OrderedDict(n=>getremoteurl(r, n; kw...) for n ∈ ns)
end

"""
    $TYPEDSIGNATURES

Add a remote named `name` to the repo pointing to the URL `url`.  $(_docstring_kwarg_notice())
"""
function addremote(r::Repo, name::AbstractString, url::AbstractString;
                   fetch::Bool=false,
                   tags::Bool=false,
                   no_tags::Bool=false,
                   kw...
                  )
    cmd = ["remote", "add"]
    fetch && push!(cmd, "-f")
    tags && push!(cmd, "--tags")
    no_tags && push!(cmd, "--no-tags")
    append!(cmd, [name, url])
    rungit(r, cmd; kw...)
    r
end

"""
    $TYPEDSIGNATURES

Gets the `stdout` of `git status` for the repo `r` as a string.  $(_docstring_kwarg_notice())
"""
function rawstatus(r::Repo, pathspec::Union{Nothing,AbstractString}=nothing;
                   short::Bool=false,
                   show_branch::Bool=false,
                   show_stash::Bool=false,
                   verbose::Bool=false,
                   mode::Union{Nothing,AbstractString}=nothing,
                   ignore_submodules::Union{Nothing,AbstractString}=nothing,
                   ignored::Union{Nothing,AbstractString}=nothing,
                   kw...
                  )
    cmd = ["status"]
    short && push!(cmd, "-s")
    show_branch && push!(cmd, "-b")
    show_stash && push!(cmd, "--show-stash")
    verbose && push!(cmd, "-v")
    isnothing(mode) || append!(cmd, ["-u", mode])
    isnothing(ignore_submodules) || push!(cmd, "--ignore-submodules=$ignore_submodules")
    isnothing(ignored) || push!(cmd, "--ignored=$ignored")
    isnothing(pathspec) || append!(cmd, ["--", pathspec])
    rungit(r, cmd; kw...)
end

"""
    $TYPEDSIGNATURES

Returns the status of the git repo `r` as an array the elements of which are the lines of `git status -s`.
$(_docstring_kwarg_notice())
"""
function status(r::Repo, pathspec::Union{Nothing,AbstractString}=nothing;
                short::Bool=true,
                kw...
               )
    st = rawstatus(r, pathspec; short, kw...)
    Iterators.map(split, eachline(IOBuffer(st)))
end

"""
    $TYPEDSIGNATURES

Prints the status as it would be returned by command line `git status`.  Additional arguments are passed to
[`rawstatus`](@ref)
"""
printstatus(io::IO, r::Repo, pathspec=nothing; kw...) = print(io, rawstatus(r, pathspec; kw...))

"""
    $TYPEDSIGNATURES

Prints the status as it would be returned by command line `git status` to `stdout`.  Additional arguments are
passed to [`rawstatus`](@ref)
"""
printstatus(r::Repo, pathspec=nothing; kw...) = printstatus(stdout, r, pathspec; kw...)

"""
    $TYPEDSIGNATURES

Get the name of the current branch of repo `r` as a string.
"""
currentbranch(r::Repo; kw...) = rungit(r, ["branch", "--show-current"]; kw...)

"""
    $TYPEDSIGNATURES

Lists commits of the repository `r`.  See also [`listcommits`](@ref).
$(_docstring_kwarg_notice())
"""
function revlist(r::Repo, branch::AbstractString="HEAD";
                 number::Union{Nothing,Integer}=nothing,
                 skip::Union{Nothing,Integer}=nothing,
                 kw...
                )
    cmd = ["rev-list", branch]
    isnothing(number) || append!(cmd, ["-n", number])
    isnothing(skip) || push!(cmd, ["--skip=$skip"])
    rungit(r, cmd; kw...)
end

"""
    $TYPEDSIGNATURES

Returns an array of commit hashes (as strings) for the repo `r`.
$(_docstring_kwarg_notice())
"""
function listcommits(r::Repo, n::Union{Nothing,Integer}=nothing;
                     branch::AbstractString="HEAD",
                     kw...
                    )
    ls = revlist(r, branch, number=n, kw...)
    split(ls, "\n")
end

"""
    $TYPEDSIGNATURES

Fetch updates from the remote names `rem` to repository `r`.
$(_docstring_kwarg_notice())
"""
function pull(r::Repo, rem::AbstractString=defaultremote(r);
              branch::AbstractString="HEAD",
              rebase::Union{Nothing,Bool,AbstractString}=nothing,
              depth::Union{Nothing,Integer}=nothing,
              prune::Bool=false,
              tags::Bool=false,
              jobs::Union{Nothing,Integer}=nothing,
              kw...
             )
    cmd = ["pull", rem, branch]
    isnothing(rebase) || append!(cmd, ["-r", string(rebase)])
    isnothing(depth) || push!(cmd, "--depth=$depth")
    prune && push!(cmd, "--prune")
    tags && push!(cmd, "--tags")
    isnothing(jobs) || append!(cmd, ["-j", string(jobs)])
    rungit(r, cmd; kw...)
    r
end

"""
    $TYPEDSIGNATURES

Add file contents to the index of repository `r`.
$(_docstring_kwarg_notice())
"""
function add(r::Repo, pathspec::AbstractVector;
             force::Bool=false,
             interactive::Bool=false,
             edit::Bool=false,
             all::Bool=false,
             kw...
            )
    cmd = ["add"]
    force && push!(cmd, "--force")    
    interactive && push!(cmd, "--interactive")
    edit && push!(cmd, "--edit")
    all && push!(cmd, "--all")
    isempty(pathspec) || append!(cmd, ["--"; pathspec])
    rungit(r, cmd; kw...)
    r
end
add(r::Repo, pathspec::AbstractString; kw...) = add(r, [pathspec]; kw...)
add(r::Repo; kw...) = add(r, []; all=true, kw...)

"""
    $TYPEDSIGNATURES

Remove files from the working tree and the index of repository `r`.
$(_docstring_kwarg_notice())
"""
function remove(r::Repo, pathspec::AbstractVector{<:AbstractString}=String[];
                force::Bool=false,
                recursive::Bool=false,
                kw...
               )
    cmd = ["rm"]
    force && push!(cmd, "--force")
    recursive && push!(cmd, "-r")
    isempty(pathspec) || append!(cmd, ["--"; pathspec])
    rungit(r, cmd; kw...)
    r
end
remove(r::Repo, pathspec::AbstractString; kw...) = remove(r, [pathspec]; kw...)

"""
    $TYPEDSIGNATURES

Record changes to repository `r`.  $(_docstring_kwarg_notice())
"""
function commit(r::Repo, pathspec::AbstractVector{<:AbstractString}=String[];
                all::Bool=false,
                author::Union{Nothing,AbstractString}=nothing,
                date=nothing,
                message::Union{Nothing,AbstractString}="GitOut.jl commit",
                kw...
               )
    cmd = ["commit"]
    all && push!(cmd, "-a")
    isnothing(author) || push!(cmd, "--author=$author")
    isnothing(date) || push!(cmd, "--date=$date")
    isnothing(message) || append!(cmd, ["-m", message])
    rungit(r, cmd; kw...)
    r
end

"""
    $TYPEDSIGNATURES

Update remotes with updates to repository `r`.  $(_docstring_kwarg_notice())
"""
function push(r::Repo, rem::AbstractString=defaultremote(r);
              branch::AbstractString="HEAD",
              delete::Bool=false,
              tags::Bool=false,
              kw...
             )
    cmd = ["push", rem, branch]
    delete && push!(cmd, "-d")
    tags && push!(cmd, "--tags")
    rungit(r, cmd; kw...)
    r
end


"""
    $TYPEDSIGNATURES

Initialize directory `dir` as a new git repo, see `man git init`. $(_docstring_kwarg_notice())
"""
function init(dir::AbstractString;
              template::Union{Nothing,AbstractString}=nothing,
              initial_branch::Union{Nothing,AbstractString}=nothing,
              bare::Bool=false,
              shared::Union{Nothing,Bool,AbstractString}=nothing,
              object_format::Union{Nothing,AbstractString}=nothing,
              kw...
             )
    cmd = ["init"]
    isnothing(template) || push!(cmd, "--template=$template")
    isnothing(initial_branch) || append!(cmd, ["-b", initial_branch])
    bare && push!(cmd, "--bare")
    isnothing(shared) || push!(cmd, "--shared=$shared")
    isnothing(object_format) || push!(cmd, "--object-format=$format")
    mkpath(dir)
    cd(() -> rungit(cmd; kw...), dir)
    Repo(dir, validate=false)
end
