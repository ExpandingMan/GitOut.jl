"""
    GitOut

A package for calling command-line git from Julia, with respect for global configuration such as
`ssh` and `git` config.

# Exports
$EXPORTS

# Imports
$IMPORTS
"""
module GitOut

using OrderedCollections
using DocStringExtensions
using URIs
import Git_jll

using Base: Process


const USE_SYSTEM_GIT = Ref(false)


_docstring_kwarg_notice(str::AbstractString="rungit") = "Additional keyword arguments are passed to [`$str`](@ref)."


include("gitcommand.jl")
include("repo.jl")


export git, rungit
export Repo
export isgitrepo, clone, remotenames, getremoteurl, remotes, status, listcommits, addremote
export printstatus, pull, add, remove, commit, push, init


end
