
_envdict(env::AbstractDict) = copy(env)
_envdict(env) = Dict(k=>v for (k,v) ∈ env)

git_exec_path(root::AbstractString=Git_jll.artifact_dir) = joinpath(root,"libexec","git-core")

git_template_dir(root::AbstractString=Git_jll.artifact_dir) = joinpath(root,"share","git-core","templates")

ssl_cert_path(root::AbstractString=dirname(Sys.BINDIR)) = joinpath(root,"share","julia","cert.pem")

function _gitenvvars!(env::AbstractDict)
    env["GIT_EXEC_PATH"] = git_exec_path()
    env["GIT_TEMPLATE_DIR"] = git_template_dir()
    env["GIT_SSL_CAINFO"] = ssl_cert_path()
    env
end

_append_args!(cmd::AbstractVector, args::AbstractVector) = append!(cmd, string.(args))

function _append_args!(cmd::AbstractVector, args)
    for (k, v) ∈ pairs(args)
        k = string(k)
        if length(k) == 1
            append!(cmd, ["-$k", string(v)])
        else
            push!(cmd, "--$k=$v")
        end
    end
end

"""
    $TYPEDSIGNATURES

Append arguments `args` to the command `cmd`.  `args` can either be an array or a key-value collection
(which must implement `Base.pairs`) such as a `NamedTuple` or an `AbstractDict`.  The keys of the
collection will be interpreted as flag names with their respective values being passed as argument
values.
"""
appendargs!(cmd::Cmd, args) = (_append_args!(cmd.exec, args); cmd)

"""
    $TYPEDSIGNATURES

Sets whether the [`git`](@ref) and [`rungit`](@ref) functions, and therefore all calls to `git` in the `GitOut`
module will use system `git` (`true`) or stand-alone git binaries from `Git_jll` (`false`).  The system `git`
is whatever `git` command is available through the shell as `git`.

`false` by default.
"""
use_system_git!(b::Bool) = (USE_SYSTEM_GIT[] = b)

"""
    $TYPEDSIGNATURES

Returns a `git` command as a `Cmd` object.

## Keyword Arguments
- `use_system`: whether or not to use system git.  See [`use_system_git!`](@ref).  Defaults to `false`
    if this was not called.
- `adjust_PATH=false`: whether or not to adjust the `PATH` variable when using `Git_jll`.
- `adjust_LIBPATH=true`: whether or not to adjust the `LIBPATH` variable when using `Git_jll`.
- `env=ENV`: A key-value collection specifying the environemnt for the command, see `Base.ENV`.
- `dir=""`: Directory for which to run the `git` command.  This uses the `-C` option for `git`.  If empty,
    the `-C` argument will not be passed.
"""
function git(;
             use_system::Bool=USE_SYSTEM_GIT[],
             adjust_PATH::Bool=false,
             adjust_LIBPATH::Bool=true,
             env=ENV,
             dir::AbstractString="",
            )
    env = _envdict(env)
    use_system || _gitenvvars!(env)
    cmd = if use_system
        `git`
    else
        Git_jll.git(;adjust_PATH, adjust_LIBPATH)
    end
    cmd = addenv(cmd, env)
    # it really doesn't want us to pass this directly to Cmd
    isempty(dir) || appendargs!(cmd, ["-C", dir])
    cmd
end

"""
    $TYPEDSIGNATURES

Create a `git` `Cmd` object as with [`git()`](@ref) but with additional arguments.  Arguments from the
`post_args` argument are always applied after those from `arg`.  This is useful for other functions which
don't provide access to `args` directly.  For example, `push(r; post_args=(tags=true,))` is equivalent to
`push(r, tags=true)`.

Arguments can be passed either as an array or a key-value collection, the keys of which are the argument
flag names.
"""
function git(args;
             post_args=(;),  # this looks silly but it's useful in constructing other commands
             kw...
            ) 
    cmd = git(;kw...)
    appendargs!(cmd, args)
    appendargs!(cmd, post_args)
end

"""
    $TYPEDSIGNATURES

Runs the command created by [`git`](@ref).

## Keyword Arguments
- `stdin=devnull`: the `stdin` of the process.
- `stdout=devnull`: the `stdout` of the process.
- `stderr=nothing`: the `stdout` of the process.  If `nothing`, a buffer will be created and used for
    error reporting.  So, for example, setting this to `devnull` will prevent showing the `stderr` in errors.
- `append=false`: Controls whether file output is appended to a file if `stdout` or `stderr` are set to a file.
- `wait=true`: Whether the thread should block until the process returns.
- `verbose_debug_logs=true`: If `stderr=nothing`, use a buffer to create a `@debug` log.

$(_docstring_kwarg_notice("git"))
"""
function rungit(::Type{Process}, args=(;);
                stdin=devnull,
                stdout=devnull,
                stderr=nothing,
                append::Bool=false,
                wait::Bool=true,
                verbose_debug_logs::Bool=true,
                kw...
               )
    cmd = git(args; kw...)
    @debug("running command:", cmd.exec)
    io_stderr = isnothing(stderr) ? IOBuffer() : stderr
    pl = pipeline(cmd; stdin, stdout, stderr=io_stderr, append)
    o = try
        run(pl; wait)
    catch e
        msg = String(take!(io_stderr))
        @error("git error: $msg")
        rethrow(e)
    end
    if verbose_debug_logs
        stderr = String(take!(io_stderr)) |> chomp
        isempty(stderr) || @debug("git command stderr:", stderr)
    end
    o
end

"""
    $TYPEDSIGNATURES

Run the `git` command and return the `stdout` as a string.
"""
function rungit(args=(;); kw...)
    io = IOBuffer()
    rungit(Process, args; stdout=io, kw...)
    String(take!(io)) |> chomp
end

